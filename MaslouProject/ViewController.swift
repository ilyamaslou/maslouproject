//
//  ViewController.swift
//  MaslouProject
//
//  Created by Ilya Maslou on 12/3/19.
//  Copyright © 2019 Ilya Maslou. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let workers: [(firstName: String, surname: String, patronymic: String, age: Int)] = [
        ("Kolya", "Pupkin", "Nikolaevich", 20),
        ("Bob", "Sponce", "SquarePants", 23),
        ("Edward", "Hardy", "Thomas", 42),
        ("Pokras", "Lampas", "Sergeevich", 28),
        ("Robin", "Cunningham", "Banksy", 45),
        ("Ivan", "Alekseev", "Aleksandrovich", 34),
        ("Christopher", "Brown", "Maurice", 30)
    ]
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var ageCountryLabel: UILabel!
    @IBOutlet private weak var positionLabel: UILabel!
    @IBOutlet private weak var experienceLabel: UILabel!
    @IBOutlet private weak var hourRateLabel: UILabel!
    @IBOutlet private weak var weekRateLabel: UILabel!
    @IBOutlet private weak var aboutMyselfTextView: UITextView!
    @IBOutlet private weak var changeUserButton: UIButton!
    @IBOutlet private weak var payButton: UIButton!
    
    @IBAction private func buttonDidChangeUser(_ sender: Any) {
        let workersCount = workers.count
        let randomWorkerIndex = Int(arc4random_uniform(UInt32(workersCount)))
        let randomWorker = workers[randomWorkerIndex]
        let ageCountryLabelOldText = ageCountryLabel.text!
            .components(separatedBy: ",")
        var oldWorkerCountry = ""
        if ageCountryLabelOldText.count > 1 {
            oldWorkerCountry = ageCountryLabelOldText[1]
        }
        
        let workerFullName = "\(randomWorker.firstName) \(randomWorker.surname) \(randomWorker.patronymic)"
        let workerExperience = Int.random(in: 1...10)
        let aboutMyself = "Hello my name is \(workerFullName). I'm \(randomWorker.age) years old. I'm from \(oldWorkerCountry)  My profession is Бизнес-аналитик. I have \(workerExperience) years of experience in this area."
        
        nameLabel.text = workerFullName
        ageCountryLabel.text = "\(randomWorker.age),\(oldWorkerCountry)"
        experienceLabel.text = "Experience: \(workerExperience) years"
        hourRateLabel.text = ""
        weekRateLabel.text = ""
        aboutMyselfTextView.text = aboutMyself
    }
    
    @IBAction private func buttonDidSetHourAndWeekRate(_ sender: Any) {
        let randomNumbers = Int.random(in: 100...500)
        weekRateLabel.text = "Week rate : \(String(randomNumbers))"
        hourRateLabel.text = "Hour rate : \(String(randomNumbers / 5))"
    }
    
}

